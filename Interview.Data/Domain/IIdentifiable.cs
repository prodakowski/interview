﻿using System;

namespace Interview.Data
{
    public interface IIdentifiable
    {
        Guid Id { get; set; }
    }
}