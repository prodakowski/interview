﻿using System;
using System.Collections.Generic;

namespace Interview.Data
{
    public interface IRepository<T> where T : IIdentifiable
    {
        IEnumerable<T> GetAll();
        T Get(Guid id);
        Guid Add(T entity);
        void Update(T entity);
        void Delete(Guid id);
    }
}
