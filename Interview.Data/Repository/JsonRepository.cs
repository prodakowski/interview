﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Interview.Data
{
    public class JsonRepository<T> : IRepository<T> where T : IIdentifiable
    {
        private static string _path;
        private object _lockObject = new object();
        private Lazy<IList<T>> _items = new Lazy<IList<T>>(() => GetAllItems(_path));

        public JsonRepository(string path)
        {
            _path = path;
        }

        public IEnumerable<T> GetAll()
        {
            return _items.Value;
        }

        public T Get(Guid id)
        {
            return _items.Value.FirstOrDefault(x => x.Id == id);
        }

        public Guid Add(T entity)
        {
            entity.Id = Guid.NewGuid();
            _items.Value.Add(entity);
            SaveToFile();
            return entity.Id;
        }

        public void Update(T entity)
        {
            var item = _items.Value.FirstOrDefault(x => x.Id == entity.Id);
            if (item == null)
            {
                throw new ArgumentException($"Object with Id = '{entity.Id}' does not exist", nameof(entity));
            }
            item = entity;
            SaveToFile();
        }

        public void Delete(Guid id)
        {
            var item = _items.Value.FirstOrDefault(x => x.Id == id);
            if (item == null)
            {
                throw new ArgumentException($"Object with Id = '{id}' does not exist");
            }
            _items.Value.Remove(item);
            SaveToFile();
        }

        private static IList<T> GetAllItems(string path)
        {
            try
            {
                var jsonText = File.ReadAllText(path);
                if (string.IsNullOrWhiteSpace(jsonText))
                {
                    return new List<T>();
                }
                return JsonConvert.DeserializeObject<T[]>(jsonText).ToList();
            }
            catch (Exception ex)
            {
                //TODO: log
                throw;
            }
        }

        private void SaveToFile()
        {
            try
            {
                lock (_lockObject)
                {
                    var jsonText = JsonConvert.SerializeObject(_items.Value, Formatting.Indented);
                    File.WriteAllText(_path, jsonText);
                }
            }
            catch (Exception ex)
            {
                //TODO: log
                throw;
            }
        }
    }
}

