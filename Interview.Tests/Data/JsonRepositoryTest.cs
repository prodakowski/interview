﻿using Interview.Data;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.IO;

namespace Interview.Tests.Data
{
    //TODO: implement remaining test

    [TestClass]
    public class JsonRepositoryTest
    {       
        private IRepository<Payment> _repository;
        private static string _testJsonFilePath = Guid.NewGuid().ToString();

        [ClassInitialize]
        public static void Init(TestContext context)
        {
            var file = File.Create(_testJsonFilePath);
            file.Close();
        }

        [TestInitialize]
        public void TestMethodInitialize()
        {
            _repository = new JsonRepository<Payment>(_testJsonFilePath);
        }

        [TestMethod]
        public void CreatePaymentTest()
        {
            var id = _repository.Add(new Payment { Amount = 123, ApplicationId = 1, ClearedDate = DateTime.Now });
            Assert.AreNotEqual(Guid.Empty, id);
        }

        [TestMethod]
        public void GetAllPaymentsTest()
        {
            var payments = _repository.GetAll();
            Assert.IsNotNull(payments);
        }

        [ClassCleanup]
        public static void CleanUp()
        {
            File.Delete(_testJsonFilePath);
        }
    }
}
