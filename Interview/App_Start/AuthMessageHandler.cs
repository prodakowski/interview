﻿using System.Net.Http;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace Interview
{
    public class AuthMessageHandler : DelegatingHandler
    {
        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            //TODO: verify user identity and set up principal

            var principal = new GenericPrincipal(new GenericIdentity("TestUser", "Forms"), new string[] { });
            HttpContext.Current.User = principal;
            Thread.CurrentPrincipal = principal;

            return base.SendAsync(request, cancellationToken);
        }
    }
}