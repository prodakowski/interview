﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web.Http;
using Interview.Data;

namespace Interview.Controllers
{
    [RoutePrefix("api/payments")]
    [Authorize]
    public class PaymentsController : ApiController
    {
        //TODO: consider dependency injection
        private IRepository<Payment> _repository = new JsonRepository<Payment>(ConfigurationManager.AppSettings["JsonFilePath"]);

        [HttpGet]
        [Route("")]
        public IEnumerable<Payment> GetList()
        {
            try
            {
                return _repository.GetAll();
            }
            catch (Exception ex)
            {
                //TODO: log
                throw;
            }
        }

        [HttpGet]
        [Route("{id:guid}")]
        public Payment Get(Guid id)
        {
            try
            {
                return _repository.Get(id);
            }
            catch (Exception ex)
            {
                //TODO: log
                throw;
            }
        }

        [HttpPut]
        public IHttpActionResult Update(Payment payment)
        {
            try
            {
                //TODO: validate arg
                var existing = _repository.Get(payment.Id);
                if (existing == null)
                {
                    return BadRequest("Payment does not exist");
                }
                _repository.Update(payment);
                return Ok();
            }
            catch (Exception ex)
            {
                //TODO: log
                return InternalServerError(ex);
            }
        }

        [HttpPost]
        public IHttpActionResult Add(Payment payment)
        {
            try
            {
                //TODO: validate arg
                _repository.Add(payment);
                return Ok();
            }
            catch (Exception ex)
            {
                //TODO: log
                return InternalServerError(ex);
            }
        }
    }
}
